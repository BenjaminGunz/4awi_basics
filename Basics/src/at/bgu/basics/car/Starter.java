package at.bgu.basics.car;



public class Starter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Engine e1 = new Engine(400, "Benzin");
		Producer p1 = new Producer("Gunz", "Austria", 5);
		Car c1 = new Car("blue", 7, 500, 15, p1, 60000);
		Person pe1 = new Person();

		System.out.println("Preis: " + c1.getBasicPrice() * ((100 - p1.getDiscount()) / 100));
		System.out.println(p1.getCountry());
		System.out.println(p1.getName());
		System.out.println(p1.getDiscount() + " % ");
		System.out.println(e1.getEngineType());
		System.out.println(e1.getHoresPower());
		System.out.println();

		c1.getUsage();
		System.out.println(c1.getUsage());
		
		for(Car curCar : pe1.getCars()) {
			System.out.println(curCar.getColor());
		}
		

	}

}
