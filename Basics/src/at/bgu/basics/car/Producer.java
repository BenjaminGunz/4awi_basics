package at.bgu.basics.car;

public class Producer {
	private String Name;
	private String Country;
	private float Discount;
	
	public Producer(String name, String country, float discount) {
		super();
		this.Name = name;
		this.Country = country;
		this.Discount = discount;
	}

	public float getDiscount() {
		return Discount;
	}

	public void setDiscount(float discount) {
		Discount = discount;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}
	
	
	
	
	
	
	
}
