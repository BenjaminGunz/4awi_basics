package at.bgu.basics.remote;

public class Remote {
	private boolean isOn = false;
	private Battery battery;
	
	
	
	
	
	
	public Remote(boolean isOn, Battery battery) {
		super();
		this.isOn = isOn;
		this.battery = battery;
		
		
	}
	
	public Battery getBattery() {
		return battery;
	}
	
	public void setBattery(Battery battery) {
		this.battery = battery;
	}

	public void turnOn(){
		this.isOn = true;
		System.out.println("I am turned on now");
	}
	
	public void turnOff(){
		this.isOn = false;
		System.out.println("I am turned off now");
	}
	
	public boolean isOn() {
		return this.isOn;
	}
	
}
